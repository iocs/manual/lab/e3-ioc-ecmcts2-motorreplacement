##############################################################################
## EtherCAT Motion Control for motor replacement in TS2

##############################################################################
## Initiation:
epicsEnvSet("IOC" ,"$(IOC="TS2-MTRREP:Ctrl-ECAT-1")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

require ecmccfg 6.3.2
require essioc

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- Choose motor record driver implementation
#-   ECMC_MR_MODULE="ecmcMotorRecord"  => ECMC native built in motor record support (Default)
#-   ECMC_MR_MODULE="EthercatMC"       => Motor record support from EthercatMC module (need to be loaded)
#- Uncomment the line below to use EthercatMC (and add optional EthercatMC_VER to startup.cmd call):
#- epicsEnvSet(ECMC_MR_MODULE,"EthercatMC")

# Epics Motor record driver that will be used:
# epicsEnvShow(ECMC_MR_MODULE)

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=6.3.2,stream_VER=2.8.10"

##############################################################################
# Configure hardware:

ecmcFileExist($(E3_CMD_TOP)/hw/ecmcMCUTS2motorreplacement.cmd,1)
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcMCUTS2motorreplacement.cmd

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

# ADDITIONAL SETUP
# Set all outputs to feed switches
#ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},BO_1,1)"
# END of ADDITIONAL SETUP

$(SCRIPTEXEC) $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=0, SAMPLE_RATE_MS=3000,FILE=$(E3_CMD_TOP)/plc/autodisable.plc, PLC_MACROS='PLC_ID=0,DBG='")

epicsEnvSet("DEV",      "$(IOC)")
##############################################################################
## AXIS 1
#
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_1.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(3,1,0,1)"

##############################################################################
## AXIS 2
#
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_2.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(4,1,0,1)"

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"

ecmcConfigOrDie "Cfg.SetDiagAxisIndex(3)"
ecmcConfigOrDie "Cfg.SetDiagAxisIndex(4)"

ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)

## Autosave configuration
##set_requestfile_path(".", "req")
##set_savefile_path(".", "save")
##set_pass0_restoreFile("positions.sav")
##save_restoreSet_DatedBackupFiles(1)
#
#save_restoreSet_Debug(1)
#save_restoreSet_IncompleteSetsOk(1)
#save_restoreSet_DatedBackupFiles(1)
#set_savefile_path(".", "save");
#set_requestfile_path(".","req")
#set_pass0_restoreFile("positions.sav")
##set_pass1_restoreFile("positions.sav")
#save_restoreSet_NumSeqFiles(3)
#save_restoreSet_SeqPeriodInSeconds(600)
#save_restoreSet_RetrySeconds(60)
#save_restoreSet_CAReconnect(1)
#save_restoreSet_CallbackTimeout(-1)


dbLoadRecords($(E3_CMD_TOP)/db/CVT_autosave.db,  "NAME=TS2-MTRREP:Ctrl-ECAT-1:Axis1")
dbLoadRecords($(E3_CMD_TOP)/db/CVT_autosave.db,  "NAME=TS2-MTRREP:Ctrl-ECAT-1:Axis2")

iocInit()

## Autosave configuration
#create_monitor_set("positions.req", 5, "P=$(IOC)")

