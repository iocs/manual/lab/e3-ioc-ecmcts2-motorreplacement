############################################################
############# TS2 motor replacement EtherCAT HW configuration

#0  0:0  PREOP  +  EK1100 EtherCAT Coupler (2A E-Bus)
#1  0:1  PREOP  +  EL9410 E-Bus Netzteilklemme (Diagnose)
#2  0:2  PREOP  +  EL7041-0052 1Ch. Stepper motor output stage (50V, 5A)
#3  0:3  PREOP  +  EL7041-0052 1Ch. Stepper motor output stage (50V, 5A)
#4  0:4  PREOP  +  EL7041-0052 1Ch. Stepper motor output stage (50V, 5A)
#5  0:5  PREOP  +  EL7041-0052 1Ch. Stepper motor output stage (50V, 5A)

#Configure EL1100 EtherCAT Coupler
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=0, HW_DESC=EK1100"

# Configure EL9410 Power supply with refresh of E-Bus.
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=1, HW_DESC=EL9410"

#Configure EL70471-0052 stepper drive terminal
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=2, HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-32.200.1.2"
# Configure reduced current 0 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"

#Configuration from IJCLab
#- Set max current to 600mA (actually max current is 1.2A for this motor)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x1,600,2)"
#- Reduced current 0mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"
#- Nominal voltage 48V (unit 1mV)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x3,48000,2)"
#- Coil resistance 1.3 Ohm (unit 10mOhm)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x4,130,2)"
#- Motor full steps count 200
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x6,200,2)"
#- Kp factor current controller 4000 (unit 0.001)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8011,0x1,4000,2)"
#- Ki factor current controller 40 (unit 0.001)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8011,0x2,40,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

#Configure EL70471-0052 stepper drive terminal
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=3, HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-52.200.2.5"
# Configure reduced current 0 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

